import utilities


def main(default=None):
    if default:
        summa = default
    else:
        summa = utilities.get_number_from_user('Give me your money')

    hryvnas, cents = str(summa).split('.')
    result = hryvnas + ' грн.'
    res_coins = utilities.get_coins_spelling_uk(int(cents))
    print(result, cents, res_coins)


if __name__ == '__main__':
    main(5.5)
