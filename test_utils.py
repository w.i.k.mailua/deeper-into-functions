import pytest

import utilities


def test_get_coins_spelling_uk_type():
    assert type(utilities.get_coins_spelling_uk(5)) == str


check_list = [
    (5, 'копійок'),
    (0, 'копійок'),
    (2, 'копійки'),
    (99, 'копійок'),
    (-99, 'копійок'),
]

@pytest.mark.parametrize('test_input,expected', check_list)
def test_get_coins_spelling_uk_value(test_input, expected):
    assert utilities.get_coins_spelling_uk(test_input) == expected
