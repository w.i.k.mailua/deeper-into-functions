import logging
from typing import Union

MSG_DEFAULT_NUMBER = 'Enter number (ex. 5.00 or 50) >>>> '


def get_coins_spelling_uk(number: int) -> str:
    last_number = int(str(number)[-1])

    if number == 0 or number in range(5, 21) or last_number == 0:
        result = 'копійок'
    elif number == 1 or last_number == 1:
        result = 'копійка'
    elif number in [2, 3, 4] or last_number in [2, 3, 4]:
        result = 'копійки'
    else:
        result = 'копійок'

    return result


def get_number_from_user(message: str = MSG_DEFAULT_NUMBER, need_int=False) -> Union[float, int]:
    while True:
        user_input = input(message)
        try:
            result = float(user_input)
            if need_int:
                return int(result)
            return result

        except ValueError:
            print('\nYour number is not appropriate. Try again')
            # logging.warning('\nYour number ins not appropriate. Try again')
